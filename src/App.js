import './App.css'
import { Button } from './components/Button'
import { Input } from './components/Input'

import tailwindTheme from './theme.json'

function App() {
  return (
    <div className="m-4 flex">
      <div className="m-4 flex w-[300px] flex-col gap-3 bg-gray-100">
        <div className="flex flex-col gap-3 border-t-4 border-gray-800 p-3">
          <div className="font-medium text-gray-600">Button</div>
          <Button variant="primary" label="Primary" />
          <Button variant="primary" disabled label="Disabled" />
          <Button variant="secondary" label="Secondary" />
          <Button variant="tertiary" label="Tertiary" />
          <Button variant="link" label="Link" />
        </div>

        <div className="flex flex-col gap-3 border-t-4 border-gray-800 p-3">
          <div className="font-medium text-gray-600">Input</div>
          <Input label="Simple Input" value="Demo Input" />
        </div>
      </div>

      <div className="m-4 w-full border-t-4  border-gray-800 bg-gray-100 p-3">
        <pre className="text-gray-900">
          {JSON.stringify(tailwindTheme, null, 2)}
        </pre>
      </div>
    </div>
  )
}

export default App
