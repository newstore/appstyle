import { twMerge } from 'tailwind-merge'

export const Button = ({ variant, label, ...props }) => (
  <button
    className={twMerge(
      'typo-body-medium flex items-center justify-center gap-2 rounded py-1 px-4 disabled:cursor-not-allowed font-semibold',

      variant === 'primary' && 'bg-gray-800 text-white',
      variant === 'secondary' && 'bg-white text-gray-900',
      variant === 'tertiary' && 'bg-transparent text-pink-500',
      variant === 'link' && 'bg-transparent text-gray-900',

      'disabled:bg-gray-200 disabled:opacity-50',
      'active:opacity-75',
    )}
    {...props}
  >
    {label}
  </button>
)
