export const Input = ({ label, value }) => (
  <label className="flex flex-col rounded border border-transparent bg-white p-3 text-black focus-within:border-blue-500">
    {Boolean(label) && (
      <div className="text-sm tracking-tight text-gray-600">{label}</div>
    )}

    <input
      value={value}
      className="mr-3 border-none text-base text-black outline-none"
    />
  </label>
)
