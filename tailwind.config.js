/** @type {import('tailwindcss').Config} */
const theme = require('./src/theme.json')

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  // associate-app/src/packages/newstore-sdk-theme/defaultVars.js
  theme: theme,
  plugins: [],
}
